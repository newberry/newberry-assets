import {
    writable
} from "svelte/store";

export const ref = writable(null);
export const active = writable(false);
export const value = writable('');
export const selectedResultIndex = writable(0);
export const events = writable([]);

export const colorRows = writable([

    {// Blue
        id: 'blue20',
        carbon: 'blue20',
        mine: 'rgb(var(--colorSplash)) lighten 40%',
        color: ''
    },{
        id: 'blue30',
        carbon: 'blue30',
        mine: 'rgb(var(--colorSplash)) lighten 30%',
        color: ''
    },{
        id: 'blue40',
        carbon: 'blue40',
        mine: 'rgb(var(--colorSplash)) lighten 20%',
        color: ''
    },{
        id: 'blue60',
        carbon: 'blue60',
        mine: 'rgb(var(--colorSplash))',
        color: 'rgb(var(--colorSplash))'
    },{
        id: 'blue70',
        carbon: 'blue70',
        mine: 'rgb(var(--colorSplash)) darken 10%',
        color: ''
    },{ // Gray
        
        id: 'gray20Hover',
        carbon: 'gray20Hover',
        mine: 'rgb(var(--colorLight)) darken +10%',
        color: ''
    },{
        id: 'gray10',
        carbon: 'gray10',
        mine: 'rgb(var(--colorLight)) darken +10%',
        color: ''
    },{
        id: 'gray20',
        carbon: 'gray20',
        mine: 'rgb(var(--colorLight)) darken +10%',
        color: ''
    },{
        id: 'gray30',
        carbon: 'gray30',
        mine: 'rgb(var(--colorLight)) darken +20%',
        color: ''
    },{
        id: 'gray40',
        carbon: 'gray40',
        mine: 'rgb(var(--colorLight)) darken +30%',
        color: ''
    },{
        id: 'gray50',
        carbon: 'gray50',
        mine: 'rgb(var(--colorLight)) darken +40%',
        color: ''
    },{
        id: 'gray60',
        carbon: 'gray60',
        mine: 'rgb(var(--colorDark)) lighten 40%',
        color: ''
    },{
        id: 'gray70',
        carbon: 'gray70',
        mine: 'rgb(var(--colorDark)) lighten 30%',
        color: ''
    },{
        id: 'gray80',
        carbon: 'gray80',
        mine: 'rgb(var(--colorDark)) lighten 20%',
        color: ''
    },{
        id: 'gray100',
        carbon: 'gray100',
        mine: 'rgb(var(--colorDark))',
        color: 'rgb(var(--colorDark)) '
    },{ // Support
        id: 'blue50',
        carbon: 'blue50',
        mine: 'rgb(var(--colorSplash)) lighten 20%',
        color: ''
    },{
        id: 'green40',
        carbon: 'green40',
        mine: 'rgb(var(--parkGreen)) lighten 10%',
        color: ''
    },{
        id: 'green50',
        carbon: 'green50',
        mine: 'rgb(var(--parkGreen))',
        color: 'rgb(var(--parkGreen))'
    },{
        id: 'yellow30',
        carbon: 'yellow30',
        mine: 'rgb(var(--sand)) darken 20%',
        color: ''
    },{
        id: 'orange40',
        carbon: 'orange40',
        mine: 'rgb(var(--flame))',
        color: 'rgb(var(--flame))'
    },{
        id: 'red50',
        carbon: 'red50',
        mine: 'rgb(var(--mulberry)) lighten 20%',
        color: ''
    },{
        id: 'red60',
        carbon: 'red60',
        mine: 'rgb(var(--mulberry)) lighten 10%',
        color: ''
    },{
        id: 'purple60',
        carbon: 'purple60',
        mine: 'rgb(????)',
        color: ''
    },{// Constants
        id: 'white',
        carbon: 'white',
        mine: 'rgb(var(--colorLight))',
        color: 'rgb(var(--colorLight))'
    },{
        id: 'whiteHover',
        carbon: 'whiteHover',
        mine: 'rgb(var(--colorLight)) darken 5%',
        color: ''
    },{
        id: 'gray80Hover',
        carbon: 'gray80Hover',
        mine: 'rgb(var(--colorDark)) lighten 25%',
        color: ''
    },{
        id: 'gray10Hover',
        carbon: 'gray10Hover',
        mine: 'rgb(var(--colorLight)) darken +15%',
        color: ''
    }])
