#!/bin/bash
# npm create svelte@latest $1
cd $1
# npm i carbon-components-svelte carbon-icons-svelte sass @sveltejs/adapter-static
cp ~/Documents/lab/newberry-assets/newberry.scss src
mkdir src/lib/comps && cp ~/Documents/lab/newberry-assets/header.svelte src/lib/comps
cp ~/Documents/lab/newberry-assets/newberry.svelte src/lib/comps
cp ~/Documents/lab/newberry-assets/svelte.config.js .
cp ~/Documents/lab/newberry-assets/+layout.svelte src/routes

